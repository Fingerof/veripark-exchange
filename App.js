/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import {
  createDrawerNavigator,
  createAppContainer,
  createStackNavigator
} from "react-navigation";
import Home from "./src/screens/Home";
import Drawer from "./src/screens/Drawer";
import React, { Component } from "react";
import SymbolDetails from "./src/screens/SymbolDetails";

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: Home,
      params: {
        option: "all",
        title: "Hisse ve Endeskler"
      }
    }
  },
  {
    contentComponent: Drawer
  }
);

const StackNavigator = createStackNavigator(
  {
    Drawer: {
      screen: DrawerNavigator
    },
    SymbolDetails: {
      screen: SymbolDetails
    }
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

const Container = createAppContainer(StackNavigator);

export default class App extends Component {
  render() {
    return <Container />;
  }
}
