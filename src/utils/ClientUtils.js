/**
 * Created by Mahmut Taşkıran on 2019-03-20 11:29
 */
const soap = require("soap-everywhere");

// Client constants
const Constants = {
  Endpoint: "http://mobileexam.veripark.com/mobileforeks/service.asmx?WSDL"
};

/**
 * Client wrapper to access soap server. Use its instance as a singleton.
 * @constructor
 */
function Session() {
  // soap client instance
  let client;
  // Encrypt's results key
  let requestedKey;
  // Initialize client object. Returns promise to use with async/await sugar.
  const initClient = () => {
    return new Promise((resolve, reject) => {
      soap.createClient(Constants.Endpoint, (err, client) => {
        if (err) return reject(err);
        resolve(client);
      });
    });
  };
  // Call Encrypt function with current time. Returns promise
  const encrypt = () => {
    if (!client) throw new Error("client is not initialized.");
    return new Promise((resolve, reject) => {
      const time = getFormattedTime();
      client.Encrypt({ request: "RequestIsValid" + time }, (err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
    });
  };
  // get default body for getForexStocksandIndexesInfo calls
  const getDefaultBody = () => {
    if (!requestedKey)
      throw new Error("requestedKey is not trusty. Call Encrypt function.");
    return {
      RequestKey: requestedKey
    };
  };
  // Call GetForexStocksandIndexesInfo function. Returns promise
  const getForexStocksandIndexesInfo = body => {
    if (!client) throw new Error("client is not initialized.");
    if (!requestedKey)
      throw new Error("requestedKey is not trusty. Call Encrypt function.");
    return new Promise((resolve, reject) => {
      body = { request: { ...getDefaultBody(), ...body } };
      client.GetForexStocksandIndexesInfo(body, (err, res) => {
        if (err) return reject(err);
        if (
          res.GetForexStocksandIndexesInfoResult[0].RequestResult.Success ===
          "true"
        ) {
          resolve(res.GetForexStocksandIndexesInfoResult[0]);
        } else {
          reject(
            res.GetForexStocksandIndexesInfoResult[0].RequestResult.Message
          );
        }
      });
    });
  };
  // get stock list
  const getStockList = async () => {
    try {
      const res = await getForexStocksandIndexesInfo();
      return {
        StocknIndexesResponseList: res.StocknIndexesResponseList.StockandIndex,
        IMKB100List: res.IMKB100List.IMKB100,
        IMKB50List: res.IMKB50List.IMKB50,
        IMKB30List: res.IMKB30List.IMKB30
      };
    } catch (e) {
      throw e;
    }
  };
  // get stock details
  const getStockDetails = async (stock, period) => {
    try {
      const res = await getForexStocksandIndexesInfo({
        Period: period,
        RequestedSymbol: stock
      });
      return [...res.StocknIndexesGraphicInfos.StockandIndexGraphic];
    } catch (e) {
      throw e;
    }
  };
  // init session object
  const initSession = async () => {
    try {
      client = await initClient();
      requestedKey = (await encrypt()).EncryptResult;
      this.isSessionInitialized = true;
    } catch (e) {
      throw e;
    }
  };
  // public methods
  return {
    initSession,
    getStockDetails,
    getStockList
  };
}

// Session instance. It is a singleton.
let singletonSession;
// Get session instance.
export default async function getSession(initialize) {
  if (!singletonSession) {
    singletonSession = new Session();
  }
  if (initialize && !singletonSession.isSessionInitialized) {
    await singletonSession.initSession();
  }
  return singletonSession;
}

/**
 * Produces formatted date
 * @return formatted date: dd:MM:yyy hh:mm
 */
function getFormattedTime() {
  const fix = n => (n.toString().length === 2 ? n : "0" + n.toString());
  const date = new Date();
  return (
    fix(date.getDate()) +
    ":" +
    fix(date.getMonth() + 1) +
    ":" +
    date.getFullYear() +
    " " +
    fix(date.getHours()) +
    ":" +
    fix(date.getMinutes())
  );
}
