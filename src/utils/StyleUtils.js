/**
 * Created by mahmuttaskiran on 2019-03-20 01:16
 */
export default {
  shadow: (
    color = "black",
    opacity = 0.5,
    radius = 10,
    offset = { width: 0, height: 4 }
  ) => ({
    shadowColor: color,
    shadowOpacity: opacity,
    shadowRadius: radius,
    shadowOffset: offset,
    elevation: offset.height
  }),
  shadowCe: (elevation, opacity = 0.5) => ({
    shadowColor: "black",
    shadowOpacity: opacity,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: elevation },
    elevation: elevation
  })
};
