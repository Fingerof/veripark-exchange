import getSession from "./ClientUtils";

function StockListWrapper() {
  const caches = {};
  let stockList = [];

  const getStockList = async refresh => {
    if (!refresh && stockList.length > 0) {
      return stockList;
    }
    const session = await getSession(true);
    stockList = await session.getStockList();
    return stockList;
  };

  // cache filtered data with unique filterKey.
  const filterStockList = async (filterKey, filter) => {
    if (stockList.length === 0)
      throw new Error("Please fetch stock list before filter it.");
    if (Array.isArray(caches[filterKey])) {
      return caches[filterKey];
    }
    const data = stockList.StocknIndexesResponseList.filter(filter);
    if (data.length > 0) {
      caches[filterKey] = data;
    }
    return data;
  };

  const releaseCache = filterKey => {
    delete caches[filterKey];
  };

  return { getStockList, filterStockList, releaseCache };
}

// it must be a singleton.
const stockListWrapper = StockListWrapper();

export default stockListWrapper;
