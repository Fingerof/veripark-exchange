/**
 * Created by mahmuttaskiran on 2019-03-22 21:34
 */
import React, { Component } from "react";
import { View, StyleSheet, Text, TextInput, BackHandler } from "react-native";
import PropTypes from "prop-types";
import StyleUtils from "../utils/StyleUtils";
import Button from "./Button";

export default class CustomToolbar extends Component {
  static propTypes = {
    onLeftIconPress: PropTypes.func,
    onRightIconPress: PropTypes.func,
    onChangeText: PropTypes.func,
    leftIcon: PropTypes.string,
    rightIcon: PropTypes.string
  };

  constructor() {
    super();
    this.state = {
      isSearching: false,
      text: ""
    };
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onHardwareBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.onHardwareBackPress
    );
  }

  onHardwareBackPress = () => {
    if (this.state.isSearching) {
      this.setState({
        text: "",
        isSearching: false
      });
      return true;
    }
    return false;
  };

  onRightIconPress = () => {
    if (this.state.isSearching) {
      this.setState(
        {
          text: "",
          isSearching: false
        },
        this.props.onChangeText
      );
    } else {
      this.setState(
        {
          text: "",
          isSearching: true
        },
        this.requestFocus
      );
    }
    if (this.props.onRightIconPress) {
      this.props.onRightIconPress();
    }
  };

  requestFocus = () => {
    if (this.textInput) {
      this.textInput.focus();
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Button.Icon
          onPress={this.props.onLeftIconPress}
          style={styles.leftIcon}
          name={this.props.leftIcon}
        />
        {this.state.isSearching ? (
          <TextInput
            ref={ref => (this.textInput = ref)}
            onChangeText={this.props.onChangeText}
            style={styles.text}
            placeholder={"Search"}
          />
        ) : (
          <Text style={styles.text}>{this.props.title}</Text>
        )}
        {this.props.rightIcon && (
          <Button.Icon
            onPress={this.onRightIconPress}
            style={styles.rightIcon}
            name={this.state.isSearching ? "close" : this.props.rightIcon}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 55,
    width: "100%",
    justifyContent: "center",
    backgroundColor: "white",
    ...StyleUtils.shadowCe(3)
  },
  leftIcon: {
    position: "absolute",
    left: 14
  },
  rightIcon: {
    right: 14,
    position: "absolute"
  },
  text: {
    left: 45,
    color: "black"
  }
});
