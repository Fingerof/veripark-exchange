/**
 * Created by @mahmuttaskiran on 2019-03-22 12:59
 */
import React, { Component } from "react";
import { View, StyleSheet, Text } from "react-native";

export default class Rows extends Component {
  static propTypes = {};

  constructor() {
    super();
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.row}>
          <Text style={styles.text}>Sembol</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.text}>Fiyat</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.text}>%Fark</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.text}>İş. Hacmi</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.text}>Alım</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.text}>Satım</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor:'#2199ff',
  },
  row: {
    flex: 1,
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 15
  },
  text: {
    color: 'white',
    borderBottomWidth: 1,
    borderBottomColor: 'white'
  }
});
