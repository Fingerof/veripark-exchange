/**
 * Created by aslitaskiran on 2019-03-20 01:17
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export function makeIcon(
  name: string,
  size: number = 18,
  color: string = 'black',
  iconset: string = 'material',
  style
) {
  return (
    <Icon
      name={name}æ
      size={size}
      color={color}
      iconset={iconset}
      style={style}
    />
  );
}

export default class Icon extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    size: PropTypes.number.isRequired,
    iconset: PropTypes.string.isRequired
  };
  static defaultProps: {
    color: 'black',
    size: 18,
    iconset: 'material'
  };
  constructor() {
    super();
  }
  render() {
    const { iconset } = this.props;
    let Class = MaterialIcon;
    if (iconset === 'material') Class = MaterialIcon;
    else if (iconset === 'community') Class = MaterialCommunityIcon;
    else if (iconset === 'fontawesome') Class = FontAwesome;
    return <Class {...this.props} />;
  }
}
