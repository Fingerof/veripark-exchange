/**
 * Created by mahmuttaskiran on 2019-03-22 21:39
 */
import React, { Component } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";

export default class Column extends Component {
  static propTypes = { data: PropTypes.object };

  constructor() {
    super();
  }

  onPress = () => {
    if (this.props.onPress) {
      this.props.onPress(this.props.data);
    }
  };

  render() {
    const { data } = this.props;
    let volumeFontSize;
    if (data.Volume.length > 10) {
      volumeFontSize = 9;
    } else if (data.Volume.length > 5) {
      volumeFontSize = 11;
    }
    let differenceColor = "red";
    if (parseFloat(data.Difference) > 0) {
      differenceColor = "#2199ff";
    }
    return (
      <TouchableOpacity
        hitSlop={{top: 5, bottom: 5, left: 5, right: 5}}
        onPress={this.onPress}
        style={[
          styles.container,
          {
            backgroundColor: this.props.single ? "white" : "#fafafa"
          }
        ]}
      >
        <View style={styles.row}>
          <Text>{data.Symbol}</Text>
        </View>
        <View style={styles.row}>
          <Text>{data.Price}</Text>
        </View>
        <View style={styles.row}>
          <Text style={{ color: differenceColor }}>{data.Difference}</Text>
        </View>
        <View style={styles.row}>
          <Text style={{ fontSize: volumeFontSize }}>{data.Volume}</Text>
        </View>
        <View style={styles.row}>
          <Text>{data.Buying}</Text>
        </View>
        <View style={styles.row}>
          <Text>{data.Selling}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxHeight: 50,
    height: 50,
    flexDirection: "row",
    alignItems: "center"
  },
  row: {
    flex: 1,
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10
  }
});
