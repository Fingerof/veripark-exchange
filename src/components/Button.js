/**
 * Created by mahmuttaskiran on 2019-03-22 21:39
 */
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import Styles from '../utils/StyleUtils';
import Icon from './Icon';

export default class Button extends Component {
  static propTypes = {
    style: PropTypes.object,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    textColor: PropTypes.string,
    backgroundColor: PropTypes.string,
    borderColor: PropTypes.string,
    borderWidth: PropTypes.number,
    borderRadius: PropTypes.number,
    shadow: PropTypes.bool,
    textSize: PropTypes.number,
    onPress: PropTypes.func
  };

  static defaultProps = {
    textColor: 'darkgrey',
    backgroundColor: 'white',
    shadow: true,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'darkgrey',
    textSize: 14
  };

  constructor() {
    super();
  }

  render() {
    return (
      <TouchableOpacity
        hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress={this.props.onPress}
        style={[
          styles.container,
          this.props.shadow ? { ...Styles.shadow() } : {},
          {
            backgroundColor: this.props.backgroundColor,
            borderWidth: this.props.borderWidth,
            borderColor: this.props.borderColor,
            borderRadius: this.props.borderRadius
          },
          this.props.style
        ]}
      >
        <Text
          style={[
            {
              fontSize: this.props.textSize,
              color: this.props.textColor
            }
          ]}
        >
          {this.props.text}
        </Text>
      </TouchableOpacity>
    );
  }
}

class Circular extends Component {
  static propTypes = {
    style: PropTypes.object,
    width: PropTypes.number.isRequired,
    backgroundColor: PropTypes.string,
    borderColor: PropTypes.string,
    borderWidth: PropTypes.number,
    shadow: PropTypes.bool,
    onPress: PropTypes.func,
    name: PropTypes.string.isRequired,
    size: PropTypes.number,
    color: PropTypes.string,
    iconset: PropTypes.string
  };

  static defaultProps = {
    textColor: 'darkgrey',
    backgroundColor: 'white',
    shadow: true,
    borderRadius: 4,
    borderWidth: 0,
    borderColor: 'darkgrey',
    textSize: 14,
    iconset: 'material',
    size: 18,
    color: 'black'
  };

  constructor() {
    super();
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        style={[
          styles.container,
          this.props.shadow ? { ...Styles.shadow() } : {},
          {
            backgroundColor: this.props.backgroundColor,
            borderWidth: this.props.borderWidth,
            borderColor: this.props.borderColor,
            width: this.props.width,
            height: this.props.width,
            borderRadius: this.props.width / 2
          },
          this.props.style
        ]}
      >
        <Icon
          color={this.props.color}
          name={this.props.name}
          size={this.props.size}
          iconset={this.props.iconset}
        />
      </TouchableOpacity>
    );
  }
}

class IconButton extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.object,
    name: PropTypes.string.isRequired,
    size: PropTypes.number,
    color: PropTypes.string,
    iconset: PropTypes.string
  };

  static defaultProps = {
    iconset: 'material',
    size: 18,
    color: 'black'
  };

  render() {
    return (
      <TouchableOpacity
        hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        style={[styles.container, this.props.style]}
        onPress={this.props.onPress}
      >
        <Icon
          name={this.props.name}
          iconset={this.props.iconset}
          size={this.props.size}
          color={this.props.color}
        />
      </TouchableOpacity>
    );
  }
}

class BorderIcon extends Component {
  static propTypes = {
    borderWidth: PropTypes.number,
    borderColor: PropTypes.string,
    onPress: PropTypes.func,
    style: PropTypes.object,
    name: PropTypes.string.isRequired,
    size: PropTypes.number,
    color: PropTypes.string,
    iconset: PropTypes.string
  };
  static defaultProps = {
    iconset: 'material',
    size: 18,
    color: 'black'
  };
  render() {
    return (
      <TouchableOpacity
        hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        style={[styles.container, this.props.style]}
        onPress={this.props.onPress}
      >
        <Icon
          name={this.props.name}
          iconset={this.props.iconset}
          style={{
            top: 0,
            left: 0,
            position: 'absolute',
            alignSelf:'center',
            transform: [{scaleX: 1.03}, {scaleY: 1.03}]
          }}
          size={this.props.size}
          color={this.props.borderColor}
        />
        <Icon
          style={{
            top: 0,
            left: 0,
            position: 'absolute',
            alignSelf:'center',
            transform: [{scaleX: 1.01}, {scaleY: 1.01}]
          }}
          name={this.props.name}
          iconset={this.props.iconset}
          size={this.props.size}
          color={this.props.color}
        />
      </TouchableOpacity>
    );
  }
}

Button.Circular = Circular;
Button.Icon = IconButton;
Button.BorderedIcon = BorderIcon;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});
