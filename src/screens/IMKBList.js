/**
 * Created by mahmuttaskiran on 2019-03-22 21:46
 */
import React, { Component } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Text
} from "react-native";
import PropTypes from "prop-types";
import CustomToolbar from "../components/CustomToolbar";
import Rows from "../components/Rows";
import Column from "../components/Column";
import stockListWrapper from "../utils/StockListWrapper";

export default class IMKBList extends Component {
  static propTypes = {
    navigation: PropTypes.object,
    option: PropTypes.string
  };

  constructor() {
    super();
    this.state = {
      isLoading: true,
      data: []
    };
  }

  filter = text => {
    // if there are a timeout to search remove it.
    if (this.timer) clearTimeout(this.timer);
    if (!text) {
      this.setState({ filteredData: null, text: "" });
      return;
    }
    // add a new timeout to avoid
    this.timer = setTimeout(this.search.bind(this, text), 500);
  };

  search = text => {
    this.setState({
      text,
      filteredData: this.state.data.filter(item => {
        if (item.Symbol.toLowerCase().includes(text.toLowerCase())) {
          return true;
        }
        return false;
      })
    });
  };

  getData = () => {
    if (Array.isArray(this.state.filteredData)) {
      return this.state.filteredData;
    }
    return this.state.data;
  };

  onStockPress = async item => {
    const total = this.state.data.filter(s => s.Symbol === item.Symbol).length;
    this.props.navigation.navigate("SymbolDetails", {
      symbol: item,
      total
    });
  };

  renderItem = ({ item, index }) => {
    return (
      <Column
        data={item}
        single={index % 2 === 0}
        onPress={this.onStockPress}
      />
    );
  };

  keyExtractor = item => {
    return item.Symbol + item.Amount + item.Price;
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = async (force = false) => {
    if (this.isLoading) return false;
    this.isLoading = true;
    this.setState({ isLoading: true });
    // Why am i using two isLoading instance?
    // isLoading that is member of this class helps us to avoiding try load data when already is loading.
    // isLoading that is state of this component help us to manage state.
    const list = await stockListWrapper.getStockList(force);
    const { option } = this.props;
    if (option === "all") {
      if (Array.isArray(list.StocknIndexesResponseList)) {
        this.setState({
          data: list.StocknIndexesResponseList,
          isLoading: false
        });
      }
    } else if (option === "grow" || option === "fall") {
      this.setState({
        data: await stockListWrapper.filterStockList(option, item => {
          return option === "grow"
            ? parseFloat(item.Difference) > 0
            : parseFloat(item.Difference) < 0;
        }),
        isLoading: false
      });
    } else if (option === "100" || option === "50" || option === "30") {
      this.setState({
        data: await stockListWrapper.filterStockList(option, item => {
          for (let inItem of list["IMKB" + option + "List"]) {
            if (inItem.Symbol === item.Symbol) {
              return true;
            }
          }
          return false;
        }),
        isLoading: false
      });
    }
    this.isLoading = false;
    return true;
  };

  componentDidUpdate(prevPros) {
    if (prevPros.option !== this.props.option) {
      this.loadData();
    }
  }

  renderEmptyListComponent = () => {
    if (this.state.filteredData) {
      return (
        <Text style={styles.emptyResultText}>
          {'No any result for "' + this.state.text + '"'}
        </Text>
      );
    }
    return null;
  };

  // @deprecated
  renderListHeaderComponent = () => {
    if (this.state.isLoading) {
      return (
        <View style={styles.headerContainer}>
          <ActivityIndicator
            style={{ marginTop: 20 }}
            size={"large"}
            color={"blue"}
            animating={true}
          />
        </View>
      );
    }
    return null;
  };

  getItemLayout = (data, index) => ({
    length: 50,
    offset: 50 * index,
    index
  });

  onLeftIconPress = () => {
    this.props.navigation.openDrawer();
  };

  onRefresh = () => {
    this.loadData(true);
  };

  render() {
    return (
      <View style={styles.container}>
        <CustomToolbar
          title={this.props.title}
          leftIcon={"menu"}
          rightIcon={"search"}
          onChangeText={this.filter}
          onLeftIconPress={this.onLeftIconPress}
        />
        {!this.state.isLoading && <Rows />}
        <FlatList
          data={this.getData()}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          onRefresh={this.onRefresh}
          refreshing={this.state.isLoading}
          initialNumToRender={10}
          maxToRenderPerBatch={10}
          getItemLayout={this.getItemLayout}
          ListEmptyComponent={this.renderEmptyListComponent}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  emptyResultText: {
    alignSelf: "center",
    textAlign: "center",
    paddingTop: 40
  },
  headerContainer: {
    width: "100%",
    height: 100,
    justifyContent: "center",
    alignItems: "center"
  }
});
