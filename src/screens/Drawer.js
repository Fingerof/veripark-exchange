/**
 * Created by mahmuttaskiran on 2019-03-22 14:54
 */
import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView
} from "react-native";
import StyleUtils from "../utils/StyleUtils";

class Option extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={[
          this.styles.container,
          { backgroundColor: this.props.isSelected ? "#e8e8e8" : "white" }
        ]}
      >
        <Text style={{ fontSize: 12 }}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }

  styles = StyleSheet.create({
    container: {
      height: 45,
      justifyContent: "center",
      paddingLeft: 15
    }
  });
}

export default class Drawer extends Component {
  static propTypes = {};

  constructor() {
    super();
    this.state = {
      currentItem: "all",
      data: [
        {
          title: "Hisse ve Endeskler",
          option: "all"
        },
        {
          title: "IMKB Yükselenler",
          option: "grow"
        },
        {
          title: "IMKB Düşenler",
          option: "fall"
        }
      ],
      dataByVolume: [
        {
          title: "Hacme Göre - 30",
          option: "30"
        },
        {
          title: "Hacme Göre - 50",
          option: "50"
        },
        {
          title: "Hacme Göre - 100",
          option: "100"
        }
      ]
    };
  }

  onOptionPress = (option, title) => {
    this.setState({
      currentItem: option
    });
    this.props.navigation.closeDrawer();
    this.props.navigation.navigate("Home", {
      option,
      title
    });
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <View style={styles.iconContainer}>
            <Text style={styles.bigTitle}>{"VERIPARK EXCHANGE"}</Text>
            <Text style={styles.smallTitle}>
              {"Source code:\nbitbucket.org/Fingerof/veripark-exchange"}
            </Text>
          </View>
          <Text style={styles.imkbWithVolumeText}>IMKB Hisse Senetleri</Text>
          {this.state.data.map(item => {
            return (
              <Option
                key={item.option}
                title={item.title}
                onPress={this.onOptionPress.bind(this, item.option, item.title)}
                isSelected={this.state.currentItem === item.option}
              />
            );
          })}
          <Text style={styles.imkbWithVolumeText}>IMKB Hacme Göre</Text>
          {this.state.dataByVolume.map(item => {
            return (
              <Option
                key={item.option}
                title={item.title}
                onPress={this.onOptionPress.bind(this, item.option, item.title)}
                isSelected={this.state.currentItem === item.option}
              />
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  iconContainer: {
    height: 150,
    backgroundColor: "#1e88e5",
    justifyContent: "space-between",
    padding: 20
  },
  bigTitle: {
    fontSize: 25,
    color: "white"
  },
  smallTitle: {
    fontSize: 10,
    color: "white"
  },
  imkbWithVolumeText: {
    marginTop: 2,
    fontWeight: "bold",
    padding: 15,
    backgroundColor: "#fafafa",
    fontSize: 12,
    ...StyleUtils.shadowCe(0.5)
  }
});
