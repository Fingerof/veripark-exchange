/**
 * Created by mahmuttaskiran on 2019-03-20 01:23
 */
import React, { Component } from "react";
import {StatusBar} from 'react-native';
import IMKBList from "./IMKBList";

export default class Home extends Component {
  constructor() {
    super();
  }
  componentDidMount() {
    StatusBar.setBackgroundColor('#2199ff')
  }
  render() {
    const { navigation } = this.props;
    return (
      <IMKBList
        title={navigation.getParam("title")}
        option={navigation.getParam("option")}
        navigation={this.props.navigation}
      />
    );
  }
}
