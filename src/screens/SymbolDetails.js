/**
 * Created by mahmuttaskiran on 2019-03-22 21:46
 */
import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  ActivityIndicator
} from "react-native";
import StyleUtils from "../utils/StyleUtils";
import Column from "../components/Column";
import Rows from "../components/Rows";
import { LineChart, YAxis, Grid } from "react-native-svg-charts";
import getSession from "../utils/ClientUtils";
import CustomToolbar from "../components/CustomToolbar";

class Detail extends Component {
  render() {
    return (
      <View style={detailsStyle.container}>
        <Text style={detailsStyle.title}>{this.props.title}</Text>
        <Text style={detailsStyle.desc}>{this.props.description}</Text>
        {this.props.showDivider && <View style={detailsStyle.divider} />}
      </View>
    );
  }
}

const detailsStyle = {
  container: {
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 7
  },
  title: {
    fontWeight: "bold"
  },
  divider: {
    width: "100%",
    height: 0.5,
    backgroundColor: "#f3f3f3",
    position: "absolute",
    left: 0,
    bottom: 0
  },
  desc: {
    marginLeft: 15
  }
};

export default class SymbolDetails extends Component {
  static propTypes = {};

  constructor() {
    super();
    this.state = {
      isLoading: true,
      period: "Day"
    };
  }

  componentDidMount() {
    // noinspection JSIgnoredPromiseFromCall
    this.fetchDetails();
  }

  fetchDetails = async () => {
    this.setState({ isLoading: true });
    const symbol = this.props.navigation.getParam("symbol");
    const session = await getSession(true);
    const details = await session.getStockDetails(
      symbol.Symbol,
      this.state.period
    );
    this.setState({ details, isLoading: false });
  };

  renderChart = () => {
    const { details } = this.state;
    const data = details.map(detail => {
      return parseFloat(detail.Price);
    });
    return (
      <View style={styles.cardContainer}>
        {this.renderPeriodFilter()}
        <View
          style={{
            flexDirection: "row",
            padding: 20,
            height: 200
          }}
        >
          <YAxis
            data={[
              Math.max(...data),
              Math.min(...data)
            ]}
            svg={{
              fill: "grey",
              fontSize: 10
            }}
            numberOfTicks={2}
          />
          <LineChart
            style={{ flex: 1, marginLeft: 16 }}
            data={data}
            svg={{ stroke: "rgb(134, 65, 244)" }}
          >
            <Grid />
          </LineChart>
        </View>
      </View>
    );
  };

  onChangePeriod = period => {
    this.setState({ period }, this.fetchDetails);
  };

  renderPeriodFilter = () => {
    return (
      <View style={styles.periodFilterContainer}>
        <Text
          onPress={this.onChangePeriod.bind(this, "Day")}
          style={{
            flex: 1,
            color: this.state.period === "Day" ? "#2199ff" : "grey",
            textAlign: "center"
          }}
        >
          {"Day"}
        </Text>
        <Text
          onPress={this.onChangePeriod.bind(this, "Week")}
          style={{
            flex: 1,
            color: this.state.period === "Week" ? "#2199ff" : "grey",
            textAlign: "center"
          }}
        >
          {"Week"}
        </Text>
        <Text
          onPress={this.onChangePeriod.bind(this, "Month")}
          style={{
            flex: 1,
            color: this.state.period === "Month" ? "#2199ff" : "grey",
            textAlign: "center"
          }}
        >
          {"Month"}
        </Text>
      </View>
    );
  };

  onBackPress = () => {
    this.props.navigation.pop();
  };

  render() {
    const symbol = this.props.navigation.getParam("symbol");
    return (
      <ScrollView contentContainerStyle={{ paddingBottom: 20 }}>
        <View style={styles.container}>
          <CustomToolbar
            leftIcon={"arrow-back"}
            onLeftIconPress={this.onBackPress}
            title={'Detaylar: ' + symbol.Symbol}
          />
          <View style={styles.cardContainer}>
            <Rows />
            <Column data={symbol} />
            <Detail
              title={"Toplam"}
              description={this.props.navigation.getParam("total")}
              showDivider={true}
            />
            <Detail
              title={"Günlük yüksek"}
              description={symbol.DayPeakPrice}
              showDivider={true}
            />
            <Detail
              title={"Günlük Düşük"}
              description={symbol.DayLowestPrice}
              showDivider={true}
            />
          </View>

          {/* render chart */}
          <View>
            {this.props.isLoading || !this.state.details ? (
              <ActivityIndicator
                animating={true}
                size={"large"}
                color={"#2199ff"}
              />
            ) : (
              this.renderChart()
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  cardContainer: {
    ...StyleUtils.shadowCe(2),
    backgroundColor: "white",
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 5,
    overflow: "hidden"
  },
  periodFilterContainer: {
    flexDirection: "row",
    height: 40,
    alignItems: "center",
    backgroundColor: "#f5f5f5"
  },
  periodFilter: {
    flex: 1
  }
});
